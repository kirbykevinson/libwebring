// @vitest-environment happy-dom

import {describe, expect, it} from "vitest";

import {Site, Ring} from "../dist/main.js";

const first = {
    "url": "first",
};
const second = {
    "name": "Second site",
    "url": "second",
    "direction": "forward"
};
const third = {
    "url": "third",
    "direction": "backward"
};
const fourth = {
    "url": "fourth",
};

const data = [first, second, third, fourth];

const hint = "second";

describe("Ring", () => {
    const ring = new Ring(data, hint);

    it("has the first member", () => {
        expect(ring.first.data).toBe(first);
    });
    it("has the current member", () => {
        expect(ring.current.data).toBe(second);
    });
    it("has the last member", () => {
        expect(ring.last.data).toBe(fourth);
    });

    it("has a random member different from the current one", () => {
        expect(ring.random).not.toBe(second);
    });

    it("can be iterated over", () => {
        let i = 0;

        for (const site of ring) {
            expect(site.data).toBe(data[i]);

            i += 1;
        }
    });
});

describe("Sites", () => {
    const ring = new Ring(data, hint);

    it("have an effective next member", () => {
        expect(ring.first.next.data).toBe(second);
    });
    it("have an actual next member", () => {
        expect(ring.first.realNext.data).toBe(second);
    });

    it("have an effective previous member", () => {
        expect(ring.last.previous.data).toBe(third);
    });
    it("have an actual previous member", () => {
        expect(ring.last.realPrevious.data).toBe(third);
    });

    it("loop backward", () => {
        expect(ring.first.previous).toBe(ring.last);
        expect(ring.first.realPrevious).toBe(ring.last);
    });
    it("loop forward", () => {
        expect(ring.last.next).toBe(ring.first);
        expect(ring.last.realNext).toBe(ring.first);
    });

    it("are skipped if can only be navigated backward", () => {
        expect(ring.current.next.data).toBe(fourth);
        expect(ring.current.realNext.data).toBe(third);
    });
    it("are skipped if can only be navigated forward", () => {
        expect(ring.current.next.previous.data).toBe(third);
        expect(ring.current.realNext.realPrevious.data).toBe(second);
    });
});

describe("Site when being installed on a link", () => {
    const ring = new Ring(data, hint);

    const withName = ring.current;
    const withoutName = ring.first;

    it("overrides the link's content and URL", () => {
        const link = document.createElement("a");

        withName.install(link);

        expect(link.innerText).toBe(withName.data.name);
        expect(link.href).toBe(withName.data.url);
    });

    it("comes up with a name if it's not present", () => {
        const link = document.createElement("a");

        withoutName.install(link);

        expect(link.innerText).toBe(withoutName.data.url);
        expect(link.href).toBe(withoutName.data.url);
    });
});

describe("Ring when being installed on a page", () => {
    const ring = new Ring(data, hint);

    it("does nothing if there's nowhere to install it on", () => {
        ring.install();

        expect(document.body.innerHTML).toBe("");
    });

    it("installs relevant Sites if they're present", () => {
        document.body.innerHTML = `
            <a id="lwr-first"></a>
            <a id="lwr-prev"></a>
            <a id="lwr-current"></a>
            <a id="lwr-random"></a>
            <a id="lwr-next"></a>
            <a id="lwr-last"></a>
        `;

        ring.install();

        const link = id => document.querySelector(`#${id}`).href;
        const url = data => data.url;

        expect(link("lwr-first")).toBe(url(first));
        expect(link("lwr-prev")).toBe(url(first));
        expect(link("lwr-current")).toBe(url(second));
        expect(link("lwr-random")).not.toBe(url(second));
        expect(link("lwr-next")).toBe(url(fourth));
        expect(link("lwr-last")).toBe(url(fourth));
    });
});
