# DO NOT USE THIS

Encouraging people to embed a third party script via hotlinking is a
horrible security practice. I don't know what I was thinking.

---

libwebring is a fully client-side JavaScript library that allows you
to build webrings using a single JSON file and a static file host.

## Quick links

* [Documentation](https://libwebring.bruh.ltd/)
* [GitLab](https://gitlab.com/warningnonpotablewater/libwebring)
* [npm](https://www.npmjs.com/package/libwebring)

## How to build

```
npm install
npm run build
```

## How to debug

```
npm run start
npm run start-docs
```

## How to use

The `example` directory inside the source code of the library contains
the most basic example of its usage. [Bruh, Ltd Official Web Ring™] is
a reference implementation of a webring using this library.

[Bruh, Ltd Official Web Ring™]: https://bruh.ltd/0000-ring/

## JSON webring specification

The file that describes the webring must be an [RFC 8259]-compliant
JSON document.

[RFC 8259]: https://www.rfc-editor.org/rfc/rfc8259

The root element of the document must be an array with at least 2
members. The ring must be considered malformed if this is not the
case. The first member should represent the website of the webring
itself.

Each member of the root array must be an object that represents a
member of the webring. The following fields are accepted:

* `url` - [RFC 3986]-compliant URI that indicates the page of the
  website, on which the webring widget is located.
* `name` - optional short description of the website. Should be
  assumed to be the same as the `url` when it is missing and needs to
  be presented to the user.
* `direction` - optional indicator of the only allowed direction, in
  which this member can be approached from. Must be either `"forward"`
  or `"backward"`.

[RFC 3986]: https://datatracker.ietf.org/doc/html/rfc3986

When navigating from one member to the next, an implementation must
skip the necessary number of members if their `direction` is set to
`backward`. The same principle applies to `direction` being set to
`forward` when navigating from one member to the previous.

A ring must be considered malformed if all of its members have their
`direction` set to the same value, as such would render the ring
unnavigable in one of the directions.

## License

[Zero-Clause BSD].

[Zero-Clause BSD]: https://gitlab.com/warningnonpotablewater/libwebring/blob/HEAD/LICENSE
